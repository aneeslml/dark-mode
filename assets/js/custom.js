let themeToggler = document.getElementById("theme-toggler");
let toggleDark = document.getElementById("toggle-label-dark");
let toggleLIGHT = document.getElementById("toggle-label-light");

themeToggler.onclick = () => {
  if (themeToggler.checked == true) {
    document.body.classList.add("active");
    toggleDark.classList.add("d-none");
    toggleLIGHT.classList.add("d-block");
  } else {
    document.body.classList.remove("active");
    toggleDark.classList.remove("d-none");
    toggleLIGHT.classList.remove("d-block");
  }
};
